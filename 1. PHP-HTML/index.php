<!DOCTYPE html>
<html>
    <head>
    <title>PHP/HTML</title>
  </head>
    <body>
    <?php   

        class CustomTable extends ArrayObject {
            
            public function __set($key, $val) {
                $this[$key] = $val;
            }

            private function getTableHeader() {
                $tr = '';
                $tr .= '<tr>';
                $tr .= '<th>KEY</th>';
                $tr .= '<th>VALUE</th>';
                $tr .= '</tr>';
            
                return $tr;
            }
            
            private function getTableData() {
                $tr = '';
                $all_data = (array) $this;   
                foreach ($all_data as $key => $val) {
                    $tr .= '<tr>';
                    $tr .= '<td>' . ucwords($key) . '</td>';
                    $tr .= '<td>' . $val . '</td>';
                    $tr .= '</tr>';
                } 

                return $tr;
            }

            public function displayAsTable() {
                $table =  '<table>';
                $table .= '<tbody>';
                $table .= $this->getTableHeader();   
                $table .= $this->getTableData();   
                $table .= '</tbody>';
                $table .=  '</table>';    
                return $table;
            } 
        }

        $obj = new CustomTable();    
        $obj->name = 'Amanat Juwel'; 
        $obj->country = 'Bangladesh'; 
        $obj->email = 'amanatjuwel@gmail.com'; 

        echo $obj->displayAsTable();    

    ?>
    </body>
</html>