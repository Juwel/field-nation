const element = document.getElementById("square");
const interval = 1000; // millisecond 
const numOfPixelMove = 10; // number of pixel on every step
let directionXaxis = 1; // 1 = move right; -1 = move left
let directionYaxis = 1; // 1 = move bottom; -1 = move up

setInterval(animateElement, interval);

function animateElement() {
    let leftPosition = element.offsetLeft,
    rightPosition = leftPosition + element.offsetWidth;

    let topPosition = element.offsetTop,
    bottomPosition = topPosition + element.offsetHeight;

    // move to opposite direction if the X axis value greater then window width
    if (rightPosition > document.body.offsetWidth) {
        directionXaxis = -1;
    }
    
    // move to opposite direction if the X axis value is negative
    if (leftPosition < 0) {
        directionXaxis = 1;
    }

    // move to opposite direction if the Y axis value greater then window height
    if (bottomPosition > document.body.offsetHeight) {
        directionYaxis = -1;
    }

    // move to opposite direction if the Y axis value is negative
    if (topPosition < 0) {
        directionYaxis = 1;
    }

    element.style.left = (leftPosition + numOfPixelMove * directionXaxis) + 'px';
    element.style.top = (topPosition + numOfPixelMove * directionYaxis) + 'px';
}

