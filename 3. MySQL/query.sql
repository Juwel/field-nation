SELECT 
      u.user_id,
      u.first_name,
      u.last_name,
      Avg(tr.correct) AS correct,
      Max(time_taken) AS time_taken
FROM  USER u
        LEFT JOIN test_result tr ON u.user_id = tr.user_id
GROUP BY u.user_id
ORDER BY u.user_id ASC; 

